## Preparar arxius per mecanitzar amb la fresadora

en funciÃ³ del tipus de mecanitzat processarem l'arxiu de diverses maneres
* tall
* gravat
* fressat 3d

software **VCarve** 
* usuari: percamlaser OEM - 245
* password: MM2RM4-GFEZYB-TJRVEW-4S56IW-AEEHE7-7UQN3J-IMVP5S-UR7JBJ-M786JK-SY8BPA

### Fressat

1. Obrir el programa VCarve

2. Crear arxiu nou

![new](https://github.com/escolamassana/manuals/blob/master/wiki/S_1.PNG)

3. Definir tamany del material que volem fressar (stock) 
- A. definir el tamany en x i y del material
- B. definir el gruix del material
- C. indicar on volem que sigui l'origen (que desprÃ©s posarem a la mÃ quina)
- D. acceptar la configuraciÃ³

![stock](https://github.com/escolamassana/manuals/blob/master/wiki/ss_1.jpg)

4. Importar arxiu
- A. archivo/Importar/Componentes i Modelo 3d (.stl, .3dm, .skp, .3ds, .obj, .dxf)

![import](https://github.com/escolamassana/manuals/blob/master/wiki/S_3.PNG)

5. Orientar el model amb el material
- A. posicionar el model amb el bloc de material digital (stock) 
- B. definir el tamany del model i centrar model i material
- C. definir posiciÃ³n del pla 0 del model

![orient](https://github.com/escolamassana/manuals/blob/master/wiki/S_4.PNG)

6. Definir els mecanitzats

![mecaniza](https://github.com/escolamassana/manuals/blob/master/wiki/S_6.PNG)










 

