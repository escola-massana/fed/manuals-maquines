# IMPRESSORES 3d

Al taller FED tenim dos tipus de cinemÃ tiques (els eixos del robot i com es rel.lacionen aquests :O) diferents:
* **cartesianes**: prusa, replicator 2, bcn3dtech
* **braÃ§os paral.lels**: anycubic delta plus


Fluxe de treball:

![alt text](https://i.all3dp.com/wp-content/uploads/2017/05/27012025/3d-printer-workflow.png "workflow")
MitjanÃ§ant una aplicaciÃ³ de modelat, escan, etc o baixant un model 3d d'internet... haurem de produir un solid "watertight" en format **stl**. 

Als pc's del taller podeu fer servir el software d'**slicing** Cura, perÃ² hi ha altres programes tambÃ© lliures molt intressants (slic3r, kisslicer, ...).

A l'hora de dissenyar-lo i "col.locar-lo" a l'Ã rea virtual s'han de tenir en compte les limitacions de la tecnologia FDM. Ãs molt Ãºtil., per aquestes tasques, el plugin de blender "3d printing tools", lliure i fÃ cil d'instal.lar.

El resultat serÃ  un arxiu en format **gcode** (instruccions en format text) que desat en una tarjeta sd, l'impressora podrÃ  anar llegint i executant (moure's, escalfar, etc).

1. amb un model 3d, generar un arxiu .stl
2. procesar-lo amb els parÃ metres dessitjats (densitat interior, alÃ§ada de capa, etc) i pel tipus de impressora seleccionada (delta? cartesiana?) amb un programa d'slicing (Cura?), per obtenir un arxiu .gcode
3. desar l'arxiu gcode en una tarja sd
4. introduir la sd en l'impressora
5. en el menÃº de l'impressora "print from sd" seleccionar l'arxiu
6. observar si l'alÃ§ada de la primera capa es adient i homegenea
(6b). en cas contrari calibrar la mÃ quina i tornar al pas 4
7. un cop finalitzada l'impressiÃ³ esperar a que refredi el bed i retirar la peÃ§a sense malmetre la superficie del bed

## DELTA
La caracterÃ­tica principal d'aquestes mÃ quines Ã©s que l'origen de coordenades, o **punt zero, es troba sobre el llit i en el seu centre**.

Al preparar el **gcode amb Cura**,  podem fer servir la configuraciÃ³ **kossel mini** sense haver de fer canvis en aquesta.

## GLOSARI
* nozle / boquilla
* hot bed / base calenta
* slicer
* gcode
* CAM
* FFM
* extruder
* hotend 
* watertight

## REFERENCIES
[open source tools for 3d printing](https://all3dp.com/1/best-free-3d-printing-software-3d-printer-program/)