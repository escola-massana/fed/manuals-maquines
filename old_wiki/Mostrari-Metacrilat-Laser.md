GRUIX 4mm

|  *  |      PotÃ¨ncia     |  Velocitat |
|----------|:-------------:|------:|
| gravat molt superficial  |  10 | 200 |
| gravat superficial |    15   |   200 |
| gravat | 30 |    200 |
| gravat profund  | 40 |    100 |
| quasi tall | 60 |    100 |
| tall | 80 |    30 |


![](https://github.com/escolamassana/manuals/blob/master/wiki/mostrari%20laser/photo5943009688893369521.jpg)

![](https://github.com/escolamassana/manuals/blob/master/wiki/mostrari%20laser/photo5943009688893369522.jpg)