MÃ quina de tall lÃ ser. Protocols de neteja i manteniment.

# Neteja de la mÃ quina lÃ ser.
**FreqÃ¼Ã¨ncia: sovint (segons Ãºs).**

Elements a revisar i/o netejar:
* Safata i travessers metÃ lÂ·lics.
* Filtre.
* Miralls i lent.

## Safata i travessers metÃ lÂ·lics.

Lâobjectiu Ã©s treure el greix acumulat en el cos intern de la mÃ quina, per acumulaciÃ³ de fum.

Materials necessaris: KH7, rotllo de paper o draps de microfibra. Una galleda amb una mica dâaigua. (com a alternativa al KH7 es pot fer servir acetona)

1. Retirar els travessers i deixar a part. 

![](https://github.com/escolamassana/manuals/blob/master/wiki/neteja-laser_06.jpeg)

2. Retirar tambÃ© el bastidor perimetral.

![](https://github.com/escolamassana/manuals/blob/master/wiki/neteja-laser_08.jpeg)

3. Treure els cargols de la safata i retirar la base foradada, per accedir a sota.

![](https://github.com/escolamassana/manuals/blob/master/wiki/neteja-laser_04.jpeg)

4. Aspirar els restes de talls de la safata i del compartiment inferior.

5. Netejar amb drap/paper i el lÃ­quid desengreixant (perÃ² sense excÃ©s dâaigua) tant la safata com les peces travesseres. 

![](https://github.com/escolamassana/manuals/blob/master/wiki/neteja-laser_09.jpeg)

6. Eixugar-les si cal abans de re colÂ·locar-les.

## Filtre.

Netejar nomÃ©s en cas que les fibres tÃ¨xtils encara estiguin en bones condicions. 
Al tercer Ãºs (aprox.) cal posar un recanvi nou.
![](https://github.com/escolamassana/manuals/blob/master/wiki/neteja-laser_11.jpeg)

Materials necessaris: Karcher dâaigua a pressiÃ³ (taller de serigrafia)

1.Retirar el filtre de la caixa.
2.Cal fer la neteja amb aigua a pressiÃ³, per el qual sâha dâanar al taller de serigrafia (a lâespai destinat per rentar les pantalles de serigrafia amb desaigÃ¼e a terra). AllÃ  hi ha una mÃ quina Karcher dâaigua a pressiÃ³.
3.Cal posar KH7 al teixit i deixar en remull abans de comenÃ§ar amb lâaigua a pressiÃ³.
4.DesprÃ©s sâha de deixar eixugar el filtre abans de tornar-ho a posar.
5.El recanvi net es deixa a lâarmari dâinstalÂ·lacions.

## Miralls i Lent.

Quan la lent i els miralls estan bruts (es veuran taques fosques a sobre de les superfÃ­cies) el tall no s'executa de forma apropiada. Cal netejar amb precauciÃ³, sÃ³n delicats.

Materials necessaris: acetona pura, bastonets de les orelles i paper per netejar Ã²ptiques.

Esquema de localitzaciÃ³ de miralls:

![](https://github.com/escolamassana/manuals/blob/master/wiki/neteja-laser_01.jpg)

1. Obrir els compartiments (quan cal) per accedir als miralls.

![](https://github.com/escolamassana/manuals/blob/master/wiki/neteja-laser_03.jpeg)
![](https://github.com/escolamassana/manuals/blob/master/wiki/neteja-laser_05.jpeg)
![](https://github.com/escolamassana/manuals/blob/master/wiki/neteja-laser_02.jpeg)

2. Treure el capÃ§al (no cal desconnectar res)

![](https://github.com/escolamassana/manuals/blob/master/wiki/neteja-laser_07.jpeg)
![](https://github.com/escolamassana/manuals/blob/master/wiki/neteja-laser_10.jpg)

3. Passar acetona fins que no es vegi taques fosques al mirall/lent, amb moviments circulars des del centre fins als extrems.




