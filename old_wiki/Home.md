## Wiki de tecnologies i les tÃ¨cniques del taller de fabricaciÃ³ i experimentaciÃ³ digital!
Escola Massana, planta 2B, Barcelona.

### DescripciÃ³
Taller enfocat a la producciÃ³ amb nous mitjans: equipat amb ordinadors per al modelat 3d i generatiu, el prototipat de visuals i sistemes interactius, materials per al desenvolupament de prototips d'eletrÃ²nica analÃ²gica, digital, interacciÃ³ tangible i virtual, i maquinÃ ria de control numÃ¨ric per a la fabricaciÃ³ digital (fresadora CNC, tall paper, lÃ ser, impressores 3d de filament fos).

### Observacions
La majoria de materials hauran de ser aportats per part de l'alumnat. Consta d'una wiki colÂ·laboratiba per a la documentaciÃ³ de mitjans de seguretat i coneixements sobre les diferents tÃ¨cniques i tecnologies que ofereix: https://github.com/escolamassana/manuals/wiki

## MANUALS DE LES MÃQUINES

lÃ ser
  * [mostrari materials laser](https://github.com/escolamassana/manuals/wiki/Mostrari-Materials---Laser)
  * [neteja mÃ quina lÃ ser](https://github.com/escolamassana/manuals/wiki/L%C3%A0ser-Neteja-i-Manteniment)

[fressadora](https://github.com/escolamassana/manuals/wiki/fressadora)

talladora

[impressores 3d](https://github.com/escolamassana/manuals/wiki/impressora3d)
