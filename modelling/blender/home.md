## Recursos

* [Blender 2.8 beginner tutorials list](https://www.youtube.com/playlist?list=PLa1F2ddGya_-UvuAqHAksYnB0qL9yWDO6)
* [a simple blender project: model, shaders, render](https://www.youtube.com/watch?v=4xLdisAvjx8&list=PLa1F2ddGya_-UvuAqHAksYnB0qL9yWDO6&index=44)

### sverchok
* tutoriales

### scripting
* [Blender scripting in python for artists](https://www.youtube.com/playlist?list=PLa1F2ddGya_8acrgoQr1fTeIuQtkSd6BW)

