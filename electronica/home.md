# Electronica

### Electròncia al taller

El taller **FED** compta amb eines i components per l'experimentació amb tecnologia electrònica, sobretot pensats per la realització de prototips d'electròncia digital: aquells sistemes que es recolzen en la programació d'un microcontrolador (un petit "ordinador").

El material del taller no es pot treure del taller sense el consentiment d'un docent. En qualsevol cas aquest prèstec quedarà registrat en aquesta pàgina de [prèstecs](/empty). 


### Inventari

#### Eines
 * [font variable](fontvariable.md)
 * oscil.loscopi
 * soldadors d'estany (6) i bases (3)
 * filtres de fum (8)
 * pelacables (4)
 * crimpadora (1)
 * protoboards (10)

#### Components
 * arduino nano (10)
 * esp32 (10)
 *  
 * l239 - driver motor
 * transistors
 * LDRs - fotoresistencia - sensor llum
 * ...

#### fungibles
 * cable unifilar
 * cable multifilar
 * estany


### Principis bàsics electrònica analògica

* llei d'ohm

* potència

