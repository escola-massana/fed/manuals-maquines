### limpieza de la máquina laser

Elements a revisar i/o netejar:

* Safata i travessers metàl·lics.
* Filtre.
* Miralls i lent.

## Safata i travessers metàl·lics

L’objectiu és treure el greix acumulat en el cos intern de la màquina, per acumulació de fum.

Materials necessaris: KH7, rotllo de paper o draps de microfibra. Una galleda amb una mica d’aigua. (com a alternativa al KH7 es pot fer servir acetona)

1. Retirar els travessers i deixar a part.
![](https://gitlab.com/escola-massana/fed/manuals-maquines/-/raw/1bd5e38af36700e0fe85c6f611d9a0f086efe074/maquines/laser/img/neteja_laser_0.jpeg "")

2. Retirar també el bastidor perimetral.
![](https://gitlab.com/escola-massana/fed/manuals-maquines/-/raw/d2a4272c7dc68d5c3667d4932d8fc694bc351465/maquines/laser/img/neteja_laser_1.jpeg "")

3. Treure els cargols de la safata i retirar la base foradada, per accedir a sota
![](https://gitlab.com/escola-massana/fed/manuals-maquines/-/raw/d2a4272c7dc68d5c3667d4932d8fc694bc351465/maquines/laser/img/neteja_laser_2.jpeg "")

4. Aspirar els restes de talls de la safata i del compartiment inferior.

5. Netejar amb drap/paper i el líquid desengreixant (però sense excés d'aigua) tant la safata com les peces travesseres.

![](https://gitlab.com/escola-massana/fed/manuals-maquines/-/raw/d2a4272c7dc68d5c3667d4932d8fc694bc351465/maquines/laser/img/neteja_laser_3.jpeg "aquí nhi ha molta ronya e")

6. Eixugar-les si cal abans de re-colocar-les

## Filtre

Netejar només en cas que les fibres tèxtils encara estiguin en bones condicions. Al tercer ús (aprox.) cal posar un recanvi nou.

![](https://gitlab.com/escola-massana/fed/manuals-maquines/-/raw/d2a4272c7dc68d5c3667d4932d8fc694bc351465/maquines/laser/img/neteja_laser_4.jpeg "ñam")

Materials necessaris: Karcher d’aigua a pressió (taller de serigrafia)

1. Retirar el filtre de la caixa. 
2. Cal fer la neteja amb aigua a pressió, per el qual s’ha d’anar al taller de serigrafia (a l’espai destinat per rentar les pantalles de serigrafia amb desaigüe a terra). Allà hi ha una màquina Karcher d’aigua a pressió. 
3. Cal posar KH7 al teixit i deixar en remull abans de començar amb l’aigua a pressió. 
4. Després s’ha de deixar eixugar el filtre abans de tornar-ho a posar. 
5. El recanvi net es deixa a l’armari d’instal·lacions.

## Miralls i lent

Quan la lent i els miralls estan bruts (es veuran taques fosques a sobre de les superfícies) el tall no s'executa de forma apropiada. Cal netejar amb precaució, són delicats.

Materials necessaris: acetona pura, bastonets de les orelles i paper per netejar òptiques.

Esquema de localització de miralls:

![](https://gitlab.com/escola-massana/fed/manuals-maquines/-/raw/d2a4272c7dc68d5c3667d49232d8fc694bc351465/maquines/laser/img/neteja_laser_5.jpg "")

1. Obrir els compartimens (quan cal) per accedir als miralls

![](https://gitlab.com/escola-massana/fed/manuals-maquines/-/raw/d2a4272c7dc68d5c3667d4932d8fc694bc351465/maquines/laser/img/neteja_laser_6.jpeg "con paciencia y saliva...")
![](https://gitlab.com/escola-massana/fed/manuals-maquines/-/raw/d2a4272c7dc68d5c3667d4932d8fc694bc351465/maquines/laser/img/neteja_laser_7.jpeg "")
![](https://gitlab.com/escola-massana/fed/manuals-maquines/-/raw/d2a4272c7dc68d5c3667d4932d8fc694bc351465/maquines/laser/img/neteja_laser_8.jpeg "")

2. Treure el capçal (no cal desconnectar res)

![](https://gitlab.com/escola-massana/fed/manuals-maquines/-/raw/d2a4272c7dc68d5c3667d4932d8fc694bc351465/maquines/laser/img/neteja_laser_9.jpeg "")
![](https://gitlab.com/escola-massana/fed/manuals-maquines/-/raw/d2a4272c7dc68d5c3667d4932d8fc694bc351465/maquines/laser/img/neteja_laser_10.jpeg "")

3. Passar acetona fins que no es vegi taques fosques al mirall/lent amb moviments circulars des del centre fins als extrems

## Canvi líquid refrigerant

S’ha de canviar cada 6 mesos. 
Aigua destilada (6 parts) i líquid refrigerant (1 part). 7 litres
