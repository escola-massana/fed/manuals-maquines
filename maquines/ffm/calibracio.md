
# Calibració impressores 3d tipus delta

Per accedir als items del menú hem de **premer** el dial, per escollir un dels items hem de **girar** i per accedir a un dels items tornarem a "
**premer** el dial. Per tornar al menú principal/submenú/pantalla d'informació podem fer servir el item superior que ens indica l'opció de "tornar".

---

Per començar necessitem instal·lar el sensor per sota de la boquilla d'extrussió, fent servir l'iman que incorpora i endollant el connector que queda lliure al costat de l'extrusor.

![sensorZ](https://gitlab.com/escola-massana/fed/manuals-maquines/raw/master/maquines/ffm/calibracio/3dprinter_calibración_0-install_probe.jpg)


## Calibratge dels paràmetres de la cinemàtica delta/braços paral·lels

Des de de la pantalla d'estat...

![sensorZ](https://gitlab.com/escola-massana/fed/manuals-maquines/raw/master/maquines/ffm/calibracio/3dprinter_calibración_0-principal.jpg)

Accedim a ...
![config](https://gitlab.com/escola-massana/fed/manuals-maquines/raw/master/maquines/ffm/calibracio/3dprinter_calibración_5-config.jpg)
![delata config](https://gitlab.com/escola-massana/fed/manuals-maquines/raw/master/maquines/ffm/calibracio/3dprinter_calibración_1-delta_config.jpg)
![auto](https://gitlab.com/escola-massana/fed/manuals-maquines/raw/master/maquines/ffm/calibracio/3dprinter_calibración_2-auto.jpg)

Aquest comandament iniciarà el procés d'autocalibratge...
![proceso2](https://gitlab.com/escola-massana/fed/manuals-maquines/raw/master/maquines/ffm/calibracio/3dprinter_calibración_3-proceso1.jpg)

![proceso1](https://gitlab.com/escola-massana/fed/manuals-maquines/raw/master/maquines/ffm/calibracio/3dprinter_calibración_3-proceso.jpg)

... i al finalitzar ens mostrarà un resultat numèric que representa la qualitat del calibratge.

![resultado](https://gitlab.com/escola-massana/fed/manuals-maquines/raw/master/maquines/ffm/calibracio/3dprinter_calibración_4-resultado.jpg)

Per desar el resultats de forma permanent...

![resultado](https://gitlab.com/escola-massana/fed/manuals-maquines/raw/master/maquines/ffm/calibracio/3dprinter_calibración_5-config.jpg)
![sensorZ](https://gitlab.com/escola-massana/fed/manuals-maquines/raw/master/maquines/ffm/calibracio/3dprinter_calibración_5-store.jpg)


## Calibratge de la superfície d'alumini ("bed") per millorar la co-planeitat de la primera capa amb el bed

Tornem a entrar al menú i selecciono...
![motion](https://gitlab.com/escola-massana/fed/manuals-maquines/raw/master/maquines/ffm/calibracio/3dprinter_calibración_6-motion.jpg)

![motion](https://gitlab.com/escola-massana/fed/manuals-maquines/raw/master/maquines/ffm/calibracio/3dprinter_calibración_7-bedlevel.jpg)

Aquest comandament iniciarà el procediment de calibratge de co-planeitat

Per desar el resultats de forma permanent...
![resultado](https://gitlab.com/escola-massana/fed/manuals-maquines/raw/master/maquines/ffm/calibracio/3dprinter_calibración_5-config.jpg)
![sensorZ](https://gitlab.com/escola-massana/fed/manuals-maquines/raw/master/maquines/ffm/calibracio/3dprinter_calibración_5-store.jpg)

## Ajust de l'alçada de la primera capa

Si, després de calibrada, a la primera capa la impressora col·lisiona o imprimeix massa alt (no aconseguim una bona adhesió ni fent servir laca sobre el bed) podem modificar el **probe Z offset** (la distància entre la punta del sensor i la boquilla).

![resultado](https://gitlab.com/escola-massana/fed/manuals-maquines/raw/master/maquines/ffm/calibracio/3dprinter_calibración_5-config.jpg)

Haurem de tornar a fer "store setting" i auto-home, al menú de "motion".


