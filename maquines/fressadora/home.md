# CNC


## Intro

## Workflow

## Seguretat


## Instruccions d'ús de la fresadora per control numèric

### Preparació de la màquina

1. Arrencar la fressadora (power on)
- girar el mando rodó vermell (imatge) fins que es desbloquegi i s'alci una mica

![powerON](https://gitlab.com/escola-massana/fed/manuals-maquines/raw/master/maquines/fressadora/preparació/1_encendre.jpg)

2. Un cop encesa fer el Homing la màquina (0,0,0) 
- A. la màquina demana si es vol fer el homing, aceptar 

![homing](https://gitlab.com/escola-massana/fed/manuals-maquines/raw/master/maquines/fressadora/preparació/3_homing.jpg)

- B. fer el homing manual 

![manualHoming](https://gitlab.com/escola-massana/fed/manuals-maquines/raw/master/maquines/fressadora/preparació/3_homing2.jpg)

3. Arrencar la refrigeració (spindle)
- Interruptor negre al mecanisme de refrigeració de darrera la fresadora

![chillerON](https://gitlab.com/escola-massana/fed/manuals-maquines/raw/master/maquines/fressadora/preparació/2_refrigeració.jpg)

4. Espai de treball, retirar el pont i fer espai per fixar el material
- Usar les fletxes verticals i horitzontals per moure el pont

![moviments](https://gitlab.com/escola-massana/fed/manuals-maquines/raw/master/maquines/fressadora/preparació/moviments.jpg)

5. Fixar la fressa escollida
- A. en funció de la fressa cambiar el "collet" (ER20)

![collet](https://gitlab.com/escola-massana/fed/manuals-maquines/raw/master/maquines/fressadora/preparació/collet.jpg)

- B. colocar la fressa al "collet" amb compte de que no caigui (es pot despuntar)

![fix1](https://gitlab.com/escola-massana/fed/manuals-maquines/raw/master/maquines/fressadora/preparació/fixació_2.jpg)

- C. apretar amb les claus fixes (27 / 30) lleugerament, no apretar molt fort

![fix2](https://gitlab.com/escola-massana/fed/manuals-maquines/raw/master/maquines/fressadora/preparació/fixació_1.jpg)

### Preparar el material a fressar

6. Fixar el material a la bancada 
- A. amb les mordasses de guia ajustant bé les altures

![mordasses](https://gitlab.com/escola-massana/fed/manuals-maquines/raw/master/maquines/fressadora/preparació/mordassa.jpg)

- B. amb un mÃ¡rtir, recomanat si s'ha de tallar peces (amb cargols o altres mecanismes de fixació que no interfereixin amb els moviments del pont)

### Definir origen màquina-material (x0, y0, z0)

7. Definir origen

- A. posicionar X i Y amb el mando de control, fent us de les fletxes, al origen de la nostre peça

![origen](https://gitlab.com/escola-massana/fed/manuals-maquines/raw/master/maquines/fressadora/preparació/origen.jpg)

- B. fixar l'ori­gen de X i de Y al mando de control amb el botó XY->0

![fixat](https://gitlab.com/escola-massana/fed/manuals-maquines/raw/master/maquines/fressadora/preparació/origenFixat.jpg)

- C. per posicionar l'origen de la Z (altura) posicionar el sensor sobre el nostre material.

![sensor](https://gitlab.com/escola-massana/fed/manuals-maquines/raw/master/maquines/fressadora/preparació/Z0.jpg)
![sensing](https://gitlab.com/escola-massana/fed/manuals-maquines/raw/master/maquines/fressadora/preparació/z.jpg)

- D. pulsar el botó C.A.D. del mando de control per fixar la altura de la Z automÃ ticament

![z](https://gitlab.com/escola-massana/fed/manuals-maquines/raw/master/maquines/fressadora/preparació/zzz.jpg)

### Mecanitzar arxiu

- A. insertar USB amb l'[arxiu](https://github.com/escolamassana/manuals/wiki/Arxiu) per mecanitzar

![usb](https://gitlab.com/escola-massana/fed/manuals-maquines/raw/master/maquines/fressadora/preparació/meca_1.jpg)

- B. accedir a la memòria pulsant F4

![open](https://gitlab.com/escola-massana/fed/manuals-maquines/raw/master/maquines/fressadora/preparació/meca_2.jpg)

- C. escollir memòria interna o USB

![memo](https://gitlab.com/escola-massana/fed/manuals-maquines/raw/master/maquines/fressadora/preparació/meca_3.jpg)

- D. escollir l'arxiu a mencanitzar (pulsar F3 per anar canviant d'arxiu) i pulsar Ok (botó verd)

![file](https://gitlab.com/escola-massana/fed/manuals-maquines/raw/master/maquines/fressadora/preparació/meca_4.jpg)

- E. al visor principal s'ha de veure el nom de l'arxiu, revisar tot i pulsar RUN (botó groc)

![run](https://gitlab.com/escola-massana/fed/manuals-maquines/raw/master/maquines/fressadora/preparació/meca_5.jpg) 



