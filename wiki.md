# Com editar aquesta wiki (gitbook)

[COM EDITAR AQUESTA WIKI (video)](https://tube.4aem.com/videos/embed/647849c2-cfe3-4c7f-8579-1a19dbfb0e98?autoplay=1)



### Síntaxis general

[Sintaxi markdown del arxius __.md__](https://docs.gitlab.com/ee/user/markdown.html#emphasis)

### Negreta/Cursiva

~~~
negreta: **text**
cursiva: __text__ (doble guió baix-text-doble guió baix)
~~~

### Links

~~~
links interns: [nom](carpeta/subcarpeta/nom_arxiu.md)
linsk externs: [nom](https://www.link.al/recurs.html)
~~~